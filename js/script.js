
$( document ).ready(function() {
   
    $('header .burger').click(function () {
        $('body').toggleClass('open-menu');
        $('.header__top__wrapper').toggleClass('active');
        $('header').toggleClass('active');
    });

    $('.menu__link__koik').click(function() {
        $('.menu__category').toggle(300);
        $(this).toggleClass('active');

    });

    $('.btn__product').click(function() {
      $('.product__description__products').toggleClass('closed');
    });

      
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#prodImage').attr('src', e.target.result);
                $('#ProductImgBackground').css('background', 'transparent');
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#fileImage").change(function(){
        readURL(this);
    });


    /*search*/

    $('#form-search input').keyup(function() {
        var val = $('#form-search input').val();
        if (val != "") {
            $('.form__search__inner').addClass('active');
        } else {
            $('.form__search__inner').removeClass('active');
        }
      });





      /*rating*/
        /* 1. Visualizing things on Hover - See next part for action on click */
        $('.prod__forms__rating a').on('mouseover', function(){
            var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
        
            // Now highlight all the stars that's not after the current hovered star
            $(this).parent().children('a.rating___num').each(function(e){
            if (e < onStar) {
                $(this).addClass('hover');
            }
            else {
                $(this).removeClass('hover');
            }
            });
            
        }).on('mouseout', function(){
            $(this).parent().children('a.rating___num').each(function(e){
            $(this).removeClass('hover');
            });
        });
        
        
        /* 2. Action to perform on click */
        $('.prod__forms__rating a').on('click', function(){
            var onStar = parseInt($(this).data('value'), 10); // The star currently selected
            var stars = $(this).parent().children('a.rating___num');
            
            for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('selected');
            }
            
            for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('selected');
            }
          
            
        });
  
  
});